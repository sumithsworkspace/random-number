--------------Setting up--------------
git clone git@bitbucket.org:sumithsworkspace/random-number.git

cd random-number

composer install

I added the .env file to make it easy

the database is SQLite. The file is inside the database dir. 
OR
[create 'database.sqlite' inside /database dir]
and 
[php artisan migrate]

in .env file
DB_CONNECTION=sqlite
DB_DATABASE=/absolute/path/to/database.sqlite

php artisan key:generate

php artisan serve
-----------------API access------------------

http://127.0.0.1:8000/api/random/generate - GET - Open

http://127.0.0.1:8000/api/random/retrieve/[id] - GET - Protected (check next steps)

http://127.0.0.1:8000/api/register - POST
name, email, password

http://127.0.0.1:8000/api/login - POST
email, password 
Access token will be returned

http://127.0.0.1:8000/api/random/retrieve/[id]
Authorization - Bearer token : Access token from login
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    
    public function register(Request $request) 
    {        
        $rules = [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails())
        {
            return response()->json(['message' => $validator->errors()],400);
        }
        
        try 
        {
            $user = User::create($request->all());
            //$token = $user->createToken('auth_token')->plainTextToken;
            $response = ['message' => 'User created', 'user' => $user];
            $response_code = 201;
        }
        catch (Exception  $e)
        {
            $response_code = $e->errorInfo[1];
            $response = ['message' => 'Record failed to save. Please try again.'];
        }
        
        return response()->json($response, $response_code);
    }

    public function login(Request $request)
    {
        
        $rules = [
            'email' => 'required|string|email',
            'password' => 'required',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json(['message' => $validator->errors()],400);
        }
        
        if (!Auth::attempt($request->only('email', 'password'))) 
        {
            return response()->json(['message' => 'Invalid login details'], 422);
        }
    
        $user = User::where('email', $request['email'])->firstOrFail();        
        $token = $user->createToken('auth_token')->plainTextToken;
        
        return response()->json(['message' => 'Login success', 'access_token' => $token,'token_type' => 'Bearer'], 201);
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return response()->json(['message' => 'Logout success'], 200);

    }
}



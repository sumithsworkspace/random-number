<?php

namespace App\Http\Controllers;

use App\Models\RandNumModel;
use Illuminate\Http\Request;
use Validator;

class RandNumController extends Controller
{
    public function randRetrieveByID($id)
    {
        $rand_num = RandNumModel::find($id);
        if(is_null($rand_num))
        {
            return response()->json(['message'=>"No record by given ID"],403);
        }
        else
        {
            return response()->json(['message'=>$rand_num],200);
        }        
    }

    public function randGenerate()
    {
        $number = RandNumModel::rand_num(); 
         
        if (RandNumModel::where('number', '=', $number)->exists())
        {
            $response = $this->randGenerate();            
        }
        else 
        {     
            try 
            {
                $rand_num = RandNumModel::create(['number' => $number]);           
                $response = response()->json(['message' => $rand_num],201);
            }
            catch (Illuminate\Database\QueryException $e)
            {
                $errorCode = $e->errorInfo[1];
                $response = response()->json(['message' => "Record failed to save. Please try again."],$errorCode);
            }            
        }
        return $response;         
    }
}

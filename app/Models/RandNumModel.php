<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RandNumModel extends Model
{
    use HasFactory;
    protected $table = "rand_num";
    protected $fillable = [
        'number',
        'created_at',
        'updated_at'
    ];

    protected function rand_num(){
        return rand(env('RAND_MIN'),env('RAND_MAX'));
    } 
}

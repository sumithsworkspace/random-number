<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RandNumController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Create users 
Route::post('/register', [AuthController::class, 'register']);

// Login and get an access_token 
Route::post('/login', [AuthController::class, 'login']);

Route::get('/random/generate', [RandNumController::class, 'randGenerate']);

//add authentication
Route::group(['middleware' => ['auth:sanctum']], function(){
    
    Route::post('/logout', [AuthController::class, 'logout']);

    Route::get('/random/retrieve/{id}', [RandNumController::class, 'randRetrieveByID']);

});
